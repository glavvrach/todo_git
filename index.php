<?php
//------------------------settings----------------------------------------------
    $host = 'localhost';
    $user = 'root';
    $pass = '';
    $useDB = 'global';// тут указывается База данных
    $useTable = 'tasks'; // тут указывается таблица
    $pdo = new PDO("mysql:$host;dbname=$useDB","$user","$pass");// подключение к БД
    //, [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]-показывает ошибки подключения к БД
    $template = file_get_contents('tenplate.html');
    $message = '<span class="default alert">Добавьте новую задачу</span>';
    $exist = NULL;
//--------------------end settings----------------------------------------------
//******************************************************************************
//--------------------functions-------------------------------------------------
function create($pdo,$task){
    global $useDB,$useTable;
                        
    try {
       $pdo->query("INSERT INTO $useDB.$useTable( description, is_done, date_added) VALUES ('$task','0',NOW())");
    }  
    catch (PDOException $e) {  
        echo $e->getMessage();
    }
    
    reload();

}
function delete($pdo,$id){
    global $useDB,$useTable;
    $pdo->query("DELETE FROM $useDB.$useTable WHERE id=$id");
    reload();
}

function duplicate(){
    reload();
}

function doIt($pdo,$id){
    global $useDB,$useTable;
    $pdo->query("UPDATE $useDB.$useTable SET `is_done`= 1 WHERE id=$id");
    reload();
}

function read($pdo,$id){
    global $useDB,$useTable;
    $result=$pdo->query("SELECT description FROM $useDB.$useTable WHERE id=$id");
    $assocArray = $result->fetch();
    //print_r($assocArray);exit;
    return $assocArray['description'];
}

function changeTask($pdo,$id,$newValue){
    global $useDB,$useTable;
    $newVal = $pdo->prepare("UPDATE $useDB.$useTable SET description = :NW WHERE id=$id");
    $newVal->execute(array(':NW'=>$newValue));
    reload();
}

function reload(){
    echo'<meta http-equiv="refresh" content="2;URL=/">';
}
//--------------------end functions---------------------------------------------
//******************************************************************************
//-------------------------form-------------------------------------------------
   

  if(isset($_GET['action'])&&($_GET['action'])=='change'){
            $val = read($pdo,intval($_GET['id']));
            $id = intval($_GET['id']);
    $formChange = '<form action="" method="POST">
                <input type="text" name="newValue" placeholder="Выберите задание" value="'.$val.'">
                <input type="submit" name="changeTask" value="Изменить задачу">
                <input type="hidden" name="id" value="'.$id.'">
                </form>';
    
            $template = str_replace('{form}',$formChange, $template);
            }else{
    $formAdd = 
               '<form action="">
                <input type="text" name="task" placeholder="Название задания">
                <input type="submit" name="create" value="Добавить">
                </form>';
                $template = str_replace('{form}',$formAdd, $template);
            }
//------------------------end form----------------------------------------------
//******************************************************************************
//-----------------------logic--------------------------------------------------
$utf8 = $pdo->query("SET NAMES 'utf8';");
        $use = $pdo->query("USE $useDB");
        if(isset($_GET['task'])&&!empty($_GET['task'])){
            $task = $_GET['task'];
        }else{
            if(isset($_GET['task'])&&empty($_GET['task'])){
            $message = '<span class="danger alert">Пустую задачу добавлять нельзя</span>';
        }
        
        }
        
        $sort = isset($_GET['selectSort'])  ? $_GET['selectSort']  : 'id';
        $allowed = array("id", "date_added", "description","is_done"); //белый список(подсмотрено на хабре)
        $key = array_search($sort,$allowed);
        $orderBy=$allowed[$key];
        //print_r();
        
       
                if(($exist == 0) &&(!empty($task))){
                    create($pdo,$task);
                    $message = '<span class="success alert">Задача успешно добавлена!</span>';
                
                }else{  
                    if($exist == 1){
                    duplicate();
                    $message = '<span class="danger alert">Такая задача уже есть!</span>';
                    }
                }
                if(isset($_GET['action'])&&(($_GET['action'])=='delete')){
                    delete($pdo,intval($_GET['id']));
                    $message = '<span class="danger alert">Задача удалена!</span>';
                }
                
                if(isset($_GET['action'])&&(($_GET['action'])=='doIt')){
                    doIt($pdo,intval($_GET['id']));
                    $message = '<span class="success alert">Задача помечена как выполненная!</span>';
                }
                
                if (isset($_POST['changeTask'])&&isset($_POST['newValue'])){
                    changeTask($pdo,intval($_POST['id']),$_POST['newValue']);
                    $message = '<span class="success alert">Задача изменена!</span>';
                }
                
                
        
//-----------------------end logic----------------------------------------------
//******************************************************************************
//-----------------------show table---------------------------------------------
  
    $template = str_replace('{message}',$message, $template);
    echo $template;
    
         $showall = $pdo->query("SELECT * FROM tasks ORDER BY $orderBy");
                foreach( $showall  as  $tasks ){
                    if($tasks['is_done']==0){
                        $isDone = 'Надо выполнять';
                        $bg = 'danger';
                }else{if($tasks['is_done']==1){
                        $isDone = 'Дело выполнено!';
                        $bg = "success";
                    
                }
                }
                    if($task==$tasks['description']){
                        $exist = 1;
                    }
                    
                    $param = $tasks['id'].'&selectSort='.$sort;
                    
                 ?>
                    <tr>
                    <td><?php echo $tasks['description'];?> </td>
                    <td class=<?php echo $bg. ' >'.$isDone;?></td>
                    <td><?php echo $tasks['date_added'];?></td>
                    <td><a href="/?action=doIt&id=<?php echo $param;?>"> Выполнить</a>&nbsp;
                    <a href="/?action=delete&id=<?php echo $param;?>"> Удалить</a>&nbsp;
                    <a href="/?action=change&id=<?php echo $param;?>"> Изменить</a>&nbsp;</td>
                    </tr>

                <?php }  //<?php echo 
//--------------------------------end table-------------------------------------                
        ?>
                 
            </tbody>
        </table>
        
        </div>
            
       </div>
        
    </body>
</html>